此文件不要删，每人新建一个自己的文件夹，将作业放入里面，其他人的文件不要动。






简易的命令行入门教程:
一、新建一个仓库，仓库名和自己空间名一模一样
	仓库名看：鼠标移到网页右上角——设置——个人资料——个人空间地址——当前个人空间地址
二、打开命令行窗口：在本地计算机选定位置，鼠标右击，选择 git bash here 
三、再做两步操作：初始化账号信息 + 下载项目
	1、全局设置gitee账号信息：
	git config --global user.name "azznbv" 全局设置用户名（请换成你的用户名）
	git config --global user.email "1841619327@qq.com" 全局设置邮箱地址（请换成你的邮箱）
	
	2、克隆下载项目到本地（使用如下命令，实际是做了两件事：下载项目+创建仓库）
	git clone+网址（换成你的仓库的地址）
		此时如果是第一次操作，会弹出登录窗口，输入你码云的账号密码，不要输错了。
		如果输错了，请打开控制面板——用户账户——管理你的凭据——windows凭据——删除gitee.com下的凭据

四、把你的站点文件传到码云仓库
	1、复制作业到仓库文件夹内。
	2、提交操作：
		git add . （不要忽略了空格和点点）把你所有改动到的文件从工作区添加到暂存区
		git commit -m "first commit" 把你的作业从暂存区，提交到本地仓库	
		git push 把你的作业从本地仓库，推送到远程仓库

至此，你的代码已从本地，传送到了你码云账号中的仓库中了。
以上就是从码云下载代码，和 把代码从本地提到码云仓库的操作。

五、如果你想要在网上可以访问你写的静态网页，那你这样操作：在你的仓库——服务——Git Pages——启动。
这样做的前提是，你仓库的根目录下，必须要有index.html文件！



命令行操作总结：设置账号信息 、 下载项目 、 提交推送代码
	         git config命令	   git clone命令    git add + git commit + git push 命令